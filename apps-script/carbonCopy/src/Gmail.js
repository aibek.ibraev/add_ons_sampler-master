// Copyright 2020 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * Callback for rendering the card for a specific Gmail message.
 * @param {Object} e The event object.
 * @return {CardService.Card} The card to show to the user.
 */
function onGmailMessage(e) {
  // Get the ID of the message the user has open.
  // var messageId = e.gmail.messageId;
  //
  // // Get an access token scoped to the current message and use it for GmailApp
  // // calls.
  // var accessToken = e.gmail.accessToken;
  // GmailApp.setCurrentMessageAccessToken(accessToken);
  //
  // // Get the subject of the email.
  // var message = GmailApp.getMessageById(messageId);
  // var subject = message.getThread().getFirstMessageSubject();
  //
  // // Remove labels and prefixes.
  // subject = subject
  //     .replace(/^([rR][eE]|[fF][wW][dD])\:\s*/, '')
  //     .replace(/^\[.*?\]\s*/, '');
  //
  // // If neccessary, truncate the subject to fit in the image.
  // subject = truncate(subject);
  //
  // return createCatCard(subject);
}

function onGmailCompose(e) {
  const draft = GmailApp.getDrafts()[0]
  const message = draft.getMessage()
  const cc = message.getCc()
  const bcc = message.getBcc()
  const textHtmlContent = '<span style="display: block; font-weight: 500;"> Carbon Copy</span> </br> ';
  var response
  if(cc.length !== 0 || bcc.length !== 0){
    var response = CardService.newUpdateDraftActionResponseBuilder()
        .setUpdateDraftBodyAction(CardService.newUpdateDraftBodyAction()
            .addUpdateContent(textHtmlContent,CardService.ContentType.MUTABLE_HTML)
            .setUpdateType(CardService.UpdateDraftBodyType.IN_PLACE_INSERT))
        .build();
  }else{
    return
  }
  return response;
}

